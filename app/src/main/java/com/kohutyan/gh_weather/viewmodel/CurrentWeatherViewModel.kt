@file:Suppress("ObjectPropertyName")

package com.kohutyan.gh_weather.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kohutyan.gh_weather.response.current.CurrentWeatherResponse
import com.kohutyan.gh_weather.response.current.Weather
import com.kohutyan.gh_weather.response.retrofit.RetrofitFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class CurrentWeatherViewModel(application: Application) : AndroidViewModel(application) {

}

    private val _currentWeather = MutableLiveData<CurrentWeatherResponse>()
    val currentWeather : LiveData<CurrentWeatherResponse> get() = _currentWeather

    fun getCurrentWeather(city: String) {
        val service = RetrofitFactory.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.getCurrentWeather("cherkasy", "id")
            try{
                withContext(Dispatchers.Main){
                    if(response.isSuccessful){
                        response.body().let {
                            _currentWeather.postValue(it)
                        }
                    }
                }
            }catch (e: HttpException){
                Log.e("REQUEST", "Exception: $e")
            }catch (e:Throwable){
                Log.e("REQUEST", "Something went wrong: $e")
        }
    }
}