package com.kohutyan.gh_weather.response.future


import com.google.gson.annotations.SerializedName

data class City(
    val country: String,
    @SerializedName("geoname_id")
    val geonameId: Int,
    val iso2: String,
    val lat: Double,
    val lon: Double,
    val name: String,
    val population: Int,
    val type: String
)