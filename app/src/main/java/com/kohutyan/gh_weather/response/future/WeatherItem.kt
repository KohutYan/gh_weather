package com.kohutyan.gh_weather.response.future

import com.kohutyan.gh_weather.response.Weather


data class WeatherItem(
    val clouds: Int,
    val deg: Int,
    val dt: Int,
    val humidity: Int,
    val pressure: Double,
    val snow: Double,
    val speed: Double,
    val temp: Temp,
    val weather: List<Weather>
)