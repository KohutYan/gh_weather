package com.kohutyan.gh_weather.response.current


import com.google.gson.annotations.SerializedName

data class Clouds(
    val all: Int
)