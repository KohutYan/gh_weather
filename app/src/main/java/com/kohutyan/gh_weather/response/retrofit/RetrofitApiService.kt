package com.kohutyan.gh_weather.response.retrofit

import com.kohutyan.gh_weather.response.current.CurrentWeatherResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

const val API_KEY = "a48d2d9cf4888e7eddf74f7a21666708"

interface RetrofitApiService {

    @GET("data/2.5/weather?")
    suspend fun getCurrentWeather(
        @Query("q") city: String,
        @Query("appid") appid: String
    ): Response<CurrentWeatherResponse>

}