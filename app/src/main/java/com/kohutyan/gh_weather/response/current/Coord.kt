package com.kohutyan.gh_weather.response.current


import com.google.gson.annotations.SerializedName

data class Coord(
    val lat: Double,
    val lon: Double
)