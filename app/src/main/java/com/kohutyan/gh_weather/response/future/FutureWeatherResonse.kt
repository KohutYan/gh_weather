package com.kohutyan.gh_weather.response.future


data class FutureWeatherResonse(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<WeatherItem>,
    val message: Int
)